function Alphabet(nilai, pesan){
    var alphaExp = /^[a-zA-Z ]+$/;
    if(nilai.value.match(alphaExp)) {
      return true;
    }
    else {
      alert(pesan);
      nilai.focus();
      return false;
    }
}
function Nomor(nilai, pesan){
    var numberExp = /^[0-9]+$/;
    if(nilai.value.match(numberExp)) {
      return true;
    }
    else {
      alert(pesan);
      nilai.focus();
      return false;
    }
}
function cekEmail(nilai, pesan){
    var email = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
    if(nilai.value.match(email)) {
      return true;
    }
    else {
      alert(pesan);
      nilai.focus();
      return false;
    }
}
function cekRadio(nilal, pesan){

    var genValue = false;

    for(var i=0; i<nilai.length;i++){
        if(nilai[i].checked == true){
            genValue = true;    
        }
    }
    if(!genValue){
        alert(pesan);
        return false;
    }

}
function validasi() {
    Alphabet(document.getElementById('inama'), 'Nama hanya berisi huruf');
    Nomor(document.getElementById('itelepon'), 'Telp hanya berisi nomor');
    cekEmail(document.getElementById('iemail'), 'Email tidak benar');
    cekRadio(document.getElementById('jk'), 'Gender tidak boleh kosong');
}