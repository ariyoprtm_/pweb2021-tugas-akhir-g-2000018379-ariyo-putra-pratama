<html>
    <head>
        <title>Tugas Akhir Main</title>
        <link rel="stylesheet" href="array.css" type="text/css">
        <script src="https://kit.fontawesome.com/8fe3757ae8.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <header>
            <div class="judul"></div>
            <h2>Tugas Akhir Pemrograman Web</h2>
            </div>
            <nav>
                <ul>
                    <li><a href="Beranda.html"><i class="fas fa-home"></i></a></li>
                    <li><a href="valid form js.html"><i class="fab fa-js"></i></a></li>
                    <li><a href="valid form php.php"><i class="fab fa-php"></i></a></li>
                    <li><a href="array dan fungsi php.php"><i class="fas fa-caret-square-right"></i></a></li>
                </ul>
            </nav>
        </header>
        <main>
            <article>
                <h1>Implementasi Array & Fungsi PHP</h1>
                <?php
                    $arrBahasa = array("Javascript","Java","HTML","PHP","Phyton");

                    echo "<b>Menampilkan isi array dengan for </b><br>";
                    for ($i=0; $i < count($arrBahasa) ; $i++) 
                    { 
                        echo $arrBahasa[$i].", ";
                    }

                    echo "<br><br>";
                    echo "<b>Menampilkan isi array dengan foreach </b><br>";
                    foreach ($arrBahasa as $program) {
                        echo $program."<br>";
                    }
                ?>
                <?php
                    echo "<b>Array asosiatif</b><br>";
                    $arrHarga = array("Pensil" => 2500, "Penghapus" => 1500, "Bolpoin" => 5000, "Penggaris" => 5500, "Pengserut" => 2000);
                    
                    echo "<b>Menampilkan  isi array asosiatif dengan foreach</b> <br>";
                    foreach($arrHarga as $alat=>$price)
                    {
                        echo "Alat Tulis $alat=$price<br>";
                    }
                    echo "<br><br>";
                ?>
                <?php
                    $arrNilai = array("Ucup" => 90, "Adit" => 98, "Kipli" => 80, "Dennis" => 85);
                
                    echo "<b><br>Array Sebelum diurutkan</b>";
                    echo "<pre>";
                    print_r($arrNilai);
                    echo "</pre>";
                
                    sort($arrNilai);
                    reset($arrNilai);
                    echo "<b>Array setelah diurutkan dengan sort()</b>";
                    echo "<pre>";
                    print_r($arrNilai);
                    echo "</pre>";
                    
                    rsort($arrNilai);
                    reset($arrNilai);
                    echo "<b>Array setelah diurutkan dengan rsort()</b>";
                    echo "<pre>";
                    print_r($arrNilai);
                    echo "</pre>";
                
                    asort($arrNilai);
                    reset($arrNilai);
                    echo "<b>Array setelah diurutkan dengan asort()</b>";
                    echo "<pre>";
                    print_r($arrNilai);
                    echo "</pre>";
                
                    arsort($arrNilai);
                    reset($arrNilai);
                    echo "<b>Array setelah diurutkan dengan arsort()</b>";
                    echo "<pre>";
                    print_r($arrNilai);
                    echo "</pre>";
                
                    ksort($arrNilai);
                    reset($arrNilai);
                    echo "<b>Array setelah diurutkan dengan ksort()</b>";
                    echo "<pre>";
                    print_r($arrNilai);
                    echo "</pre>";
                
                    krsort($arrNilai);
                    reset($arrNilai);
                    echo "<b>Array setelah diurutkan dengan krsort()</b>";
                    echo "<pre>";
                    print_r($arrNilai);
                    echo "</pre>";
                ?>
                <?php
                    //Fungsi tanpa return value & parameter
                    echo "<b>Fungsi tanpa return value & parameter menampilkan bilangan prima 2-100<br></b>";
                    function cetak_prima(){
                        for ($x=1; $x <= 100 ; $x++) { 
                            $a = 0;
                            for ($y=1; $y <= $x ; $y++) { 
                                if ($x % $y == 0) {
                                    $a++;
                                }
                            }
                            if ($a == 2) {
                                echo $x."<br>";
                            }
                        }
                    }
                    cetak_prima();
                    echo "<br><br>";
                ?>
                <?php
                    //Fungsi tanpa return value tapi dengan parameter
                    echo "<b>Fungsi tanpa return value tapi dengan parameter menampilkan bilangan prima 2-50<br></b>";
                    function cetak_primawp($awal,$akhir){
                        for ($x=$awal; $x <= $akhir ; $x++) { 
                            $a = 0;
                            for ($y=1; $y <= $x ; $y++) { 
                                if ($x % $y == 0) {
                                    $a++;
                                }
                            }
                            if ($a == 2) {
                                echo $x."<br>";
                            }
                        }
                    }

                    $v = 2;
                    $w = 50;
                    cetak_primawp($v,$w);
                    echo "<br><br>";
                ?>
                <?php

                    //Fungsi dengan Return Value dan Parameter
                    echo "<b>Fungsi dengan return value dan parameter<br></b>";
                    function kel_lingkaran($jari){
                        return 3.14*2*$jari;
                    }

                    $r = 14;
                    echo "Keliling lingkaran dengan jari-jari $r : ";
                    echo kel_lingkaran($r);
                    echo "<br><br>";
                ?>                
            </article>
        </main>
        <div class="hapus">
        </div>
        <footer>
           <p> Copyright <i class="far fa-copyright"></i> Ariyo Putra Pratama | 2000018379 </p>
        </footer>
    </body>
</html>