<html>
    <head>
        <title>Tugas Akhir Main</title>
        <link rel="stylesheet" href="php.css" type="text/css">
        <script src="https://kit.fontawesome.com/8fe3757ae8.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <header>
            <div class="judul"></div>
            <h2>Tugas Akhir Pemrograman Web</h2>
            </div>
            <nav>
                <ul>
                    <li><a href="Beranda.html"><i class="fas fa-home"></i></a></li>
                    <li><a href="valid form js.html"><i class="fab fa-js"></i></a></li>
                    <li><a href="valid form php.php"><i class="fab fa-php"></i></a></li>
                    <li><a href="array dan fungsi php.php"><i class="fas fa-caret-square-right"></i></a></li>
                </ul>
            </nav>
        </header>
        <main>
            <article>
            <h1>Penanganan Form PHP</h1>
            <?php
                $valnama = $valalamat = $valtelp = $valemail = $valgender = "";
                $nama = $telp = $mail = $gender = $saran = "";
                $alamat = ($_POST["ialamat"]);
                $asal = ($_POST["iasal"]);

                if ($_SERVER["REQUEST_METHOD"] == "POST" ) 
                {
                    if (empty($_POST["inama"])) 
                    {
                        $valnama = "Nama Harus diisi";    
                    }
                    else 
                    {
                        $nama = test_input($_POST["inama"]);
                        
                        if (!preg_match("/^[a-zA-Z ]*$/",$nama)) 
                        {
                            $valnama = "Hanya boleh diisi Huruf dan Spasi";
                        }
                    }
                    
                    if (empty($_POST["ialamat"])) 
                    {
                        $valalamat = "Alamat Harus diisi";
                    }

                    if (empty($_POST["itelepon"])) 
                    {
                        $valtelp = "Nomor Telepon Harus diisi";
                    }
                    else 
                    {
                        $telp = test_input($_POST["itelepon"]);

                        if (!preg_match("/^[0-9]*$/",$telp)) 
                        {
                            $valtelp = "Hanya boleh diisi Angka";
                        }
                    }

                    if (empty($_POST["imail"])) 
                    {
                        $valtelp = "Email Harus diisi";
                    }
                    else 
                    {
                        $mail = test_input($_POST["imail"]);

                        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) 
                        {
                            $valemail = "Email Belum Benar";
                        }
                    }

                    if (empty($_POST["jk"])) 
                    {
                        $valgender = "Gender Harus diisi";
                    }
                    else 
                    {
                        $gender = test_input($_POST["jk"]);
                    }
                    
                    if (empty($_POST["isaran"])) 
                    {
                        $saran = "";
                    }
                    else 
                    {
                        $saran = test_input($_POST["isaran"]);
                    }
                }

                function test_input($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                }
        ?>
                    
                <table cellspacing="5px" padding="8px" align="center">
                    <br>
                    <tr>
                        <th colspan=3>
                        <div id="judul">
                        </th>
                    </tr>
                    </div>
                    <tr>
                        <td colspan=3>
                            <p>
                            <span class ="error">
                                * required data
                            </span>
                            </p>
                        </td>
                    </tr>
                    <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" id="kotak-bio"> 
                    <tr>
                        <td style="width: 20px;"><label>Nama</label></td>
                        <td style="width: 110px;font-size: 20pt;font-family: serif;">:</td>
                        <td style="237px">
                            <input name="inama" id ="inama" size="30" type="text" value="<?php echo $nama;?>">
                            <span class="error">
                                * <?php echo $valnama;?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Alamat</label></td>
                        <td style="font-size: 20pt;font-family: serif;color;">:</td>
                        <td>
                            <input name="ialamat" id="ialamat" size="30" type="text" value="<?php echo $alamat;?>">
                            <span class="error">
                                * <?php echo $valalamat;?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Telepon</label></td>
                        <td style="font-size: 20pt;font-family: serif;">:</td>
                        <td>
                            <input name="itelepon" id="itelepon" size="30" type="text" value="<?php echo $telp;?>">
                            <span class="error">
                                * <?php echo $valtelp;?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Email</label></td>
                        <td style="font-size: 20pt;font-family: serif;">:</td>
                        <td>
                            <input name="imail" id="imail" size="30" type="text" value="<?php echo $mail;?>">
                            <span class="error">
                                * <?php echo $valemail;?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Jenis Kelamin</label></td>
                        <td style="font-size: 20pt;font-family: serif;">:</td>
                        <td>
                            <input name="jk" type="radio" <?php 
                            if (isset($gender) && $gender == "Laki-laki") echo "checked";?> value="Laki-laki">Laki-laki<br>
                            <input name="jk" type="radio" <?php 
                            if (isset($gender) && $gender == "Perempuan") echo "checked";?> value="Perempuan">Perempuan<br>
                            <span class="error">
                                * <?php echo $valgender;?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Asal</label></td>
                        <td style="font-size: 20pt;font-family: serif;">:</td>
                        <td><select name="iasal" id="iasal">
                            <option>--Asal--</option>
                            <option>Luar DIY Yogyakarta</option>
                            <option>DIY Yogyakarta dan Sekitarnya</option>
                        </select></td>
                    </tr>
                    <tr>
                        <td colspan=3 align="center">
                            <input type="textarea" name="isaran" id="isaran" placeholder="Kritik dan Saran" style="height: 100px;" size="50">
                            <?php echo $saran;?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=3 align="center">
                            <input id=kumpul type="submit" value="SIMPAN">
                            <input id=batal type="reset" value="BATAL">
                        </td>
                    </tr>
                    </form>
                    <tr>
                        <td colspan=3>
                        <div id="hasil">
                            <?php
                                echo"<br><br>";
                                echo "<h2><center>Data Biodata :</h2>";
                                echo "Nama : $nama";
                                echo "<br><br>";
                                echo "Alamat : $alamat";
                                echo "<br><br>";
                                echo "Telp : $telp";
                                echo "<br><br>";
                                echo "Email : $mail";
                                echo "<br><br>";
                                echo "Jenis Kelamin : $gender";
                                echo "<br><br>";
                                echo "Asal : $asal";
                                echo "<br><br>";
                                echo $saran;
                                echo "</center>";
                            ?>
                        </td>
                        </div>
                    </tr>    
                </table>
            </article>
        </main>
        <div class="hapus">
        </div>
        <footer>
           <p> Copyright <i class="far fa-copyright"></i> Ariyo Putra Pratama | 2000018379 </p>
        </footer>
    </body>
</html>